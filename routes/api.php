<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::group(['prefix' => 'auth'], function () {
    Route::post('register', 'AuthController@register');
    Route::post('login', 'AuthController@login')->name('login');
    Route::post('refresh ', 'AuthController@refresh');
    Route::post('logout ', 'AuthController@logout');
    Route::get('user ', 'AuthController@user');

});

Route::group(['middleware' => ['jwt.verify']], function() {

	Route::post("util/getPasien", "Api\UtilController@pasien");
	Route::get("calonpasien", "Api\PasienController@calon");
	Route::post("calonpasien/create", "Api\PasienController@store");
	Route::post("calonpasien/delete" ,"Api\PasienController@destroy");
	Route::post("calonpasien/update", "Api\PasienController@update");
	Route::get("pasien", "Api\PasienController@index");
	Route::post("form-harian","Api\PasienController@create");

	Route::post("promo/store", "Api\PromoController@store");
	Route::get("promo","Api\PromoController@index");
	Route::post("promo/delete" ,"Api\PromoController@destroy");
	Route::get("promo-kategori","Api\PromoController@kategoriPasien");
	Route::post("promo/update", "Api\PromoController@update");
	Route::get("param/variable","Api\UtilController@allParam");
	Route::post("param/hpl","Api\UtilController@storeHpl");
	Route::post("param/batas-akhir","Api\UtilController@storeBatasNotifBumil");
	Route::post("param/reminder-baby","Api\UtilController@storeReminderBaby");
	Route::post('send-promo',"Api\PromoController@sendpromo");
	Route::get('log-promo','Api\PromoController@logpromo');
	route::get('dashboard','Api\UtilController@mobileDashboard');
});

