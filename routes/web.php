<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('login-show', 'DashboardController@login_view')->name('login.show');
Route::post('login-web','DashboardController@login_web')->name('login.web');
Auth::routes();

Route::group(['prefix' => '/','middleware' => 'auth:web'], function () {

	Route::get('error-404',function () {
	    return view('error404');
	})->name('error.404');

	Route::get('/', function(){
		return redirect('dashboard');
	});
	
	Route::get('dashboard', 'DashboardController@index')->name('dashboard');
	Route::post('dashboard', 'DashboardController@index')->name('dashboard');
	Route::get('dashboard/promo-bulanan', 'DashboardController@promoKedatangan')->name('dashboard.promo');
	Route::get('dashboard/kedatangan','DashboardController@kedatangan')->name('dashboard.kedatangan');
	Route::get('logout','DashboardController@logout');
	Route::group(['middleware' => 'auth.admin'], function () {
		Route::get('user-admin','AdminController@index');
		Route::get('user-add','AdminController@create');
		Route::post('user-store','AdminController@store')->name('user.store');
		Route::get('user-edit/{id}','AdminController@edit')->name('user.edit');
		Route::post('user-update/{id}','AdminController@update')->name('user.update');
		Route::get('user-deleted/{id}','AdminController@destroy')->name('user.destroy');

		Route::get('setting','SettingController@index');
		Route::post('setting-update/{id}','SettingController@update')->name('setting.update');
		Route::post('setting-keywoo','SettingController@wookey')->name('setting.wookey');
		Route::resource('pasien', 'PasienController');
		Route::post('pasien-add/{id}','PasienController@jadikanPasien')->name('pasien.add');
		
		Route::resource('kategory','KategoryController');
		Route::resource('reminder', 'ReminderController');
		Route::resource('promo','PromoController');

		Route::post('promo-send/{id}','PromoController@kirimPromo')->name("promo.send");

		Route::get("laporan/logPromo",'LaporanController@logPromo')->name("log.promo");
		Route::get("laporan/logRemin",'LaporanController@logReminder')->name("log.remin");



	});
	
});

