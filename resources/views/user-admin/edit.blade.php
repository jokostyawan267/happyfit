
@extends('layouts.app')

@section('content')
 	<div class="text-center">
        <h1 class="h4 text-gray-900 mb-4">Edit User</h1>
    </div>
    <hr>
    @if ($errors->has('others'))
    <div class="alert alert-danger" role="alert" style="margin-left: 32px;margin-right: 32px;">
		  {{ $errors->first('others') }}
	</div>
	@endif
    <div class="col-md-12">
    	<form action="{{ route('user.update',$data->id) }}" method="post" autocomplete="off" role="presentation">
    		@csrf
			  <div class="form-group">
			    <label for="exampleInputName1">Nama</label>
			    <input type="text" class="form-control" id="exampleInputName1" aria-describedby="emailHelp" name="name" value="{{ $data->name }}" placeholder="Enter Name" required>
			     @if ($errors->has('name'))
			     <small id="emailHelp" class="form-text text-danger">{{ $errors->first('name') }}</small>
			     @endif
			  </div>
			  <div class="form-group">
			    <label for="exampleInputEmail1">Email address</label>
			    <input type="email" class="form-control" id="exampleInputEmail1" aria-describedby="emailHelp" readonly name="email" value="{{ $data->email }}" placeholder="Enter email" required>
			    @if ($errors->has('email'))
			     <small id="emailHelp" class="form-text text-danger">{{ $errors->first('email') }}</small>
			     @endif
			  </div>
			  <div class="form-group">
			    <label for="exampleInputPassword1">Password</label>
			    <input type="password" class="form-control" id="exampleInputPassword1" name="password" placeholder="Ubah Password" readonly onfocus="this.removeAttribute('readonly');" >
			    @if ($errors->has('password'))
			     <small id="emailHelp" class="form-text text-danger">{{ $errors->first('password') }}</small>
			     @endif
			  </div>
			  <div class="form-group">
			    	<div class="form-check form-check-inline">
						<input class="form-check-input" type="radio" name="role" id="inlineRadio1" value="admin" @if($data->hak_akses->role == 'admin' ) checked @endif  required>
					    <label class="form-check-label" for="inlineRadio1">Admin</label>
					</div>
					<div class="form-check form-check-inline">
					  <input class="form-check-input" type="radio" name="role" id="inlineRadio2" value="user" @if($data->hak_akses->role == 'user' ) checked @endif required>
					  <label class="form-check-label" for="inlineRadio2">Staff</label>
					</div>
				
			  </div>
			  <button type="submit" class="btn btn-primary">Submit</button>
		</form>
    </div>
    
@endsection
