 
@extends('layouts.app')

@section('content')
 <!-- Page Heading -->

<!-- DataTales Example -->
    <div class="card shadow mb-4">
    <div class="card-header py-3">
        <h6 class="m-0 font-weight-bold text-primary">Log Reminder pasien</h6>
       
    </div>
    <div class="card-body">
        <div class="col-md-6" >
            <div class="form-group row">
                <label for="staticEmail" class="col-sm-2 col-form-label">Tanggal</label>
                <div class="col-sm-10">
                  <div id="reportrange" class="form-control-plaintext" style="background: #fff; cursor: pointer; padding: 5px 10px; border: 1px solid #ccc; width: 100%">
                    <i class="fa fa-calendar"></i>&nbsp;
                    <span></span> <i class="fa fa-caret-down"></i>
                </div>
                </div>
            </div>
            <div class="form-group row">
                <label for="staticEmail" class="col-sm-2 col-form-label">Cari</label>
                <div class="col-sm-10">
                  <input type="text" name="_q" id="_q" class="form-control">
                </div>
            </div>
            
        </div>
        <hr>
       
        <div class="table-responsive">
            <table class="table table-bordered dataTable table-center" id="dataTable" width="100%" cellspacing="0">
                <thead>
                    <tr>
                        <th>Reminder</th>
                        <th>Nomor WhatsApp</th>
                        <th>Nama Pasien</th>
                        <th>Jenis</th>
                        <th>Tanggal Kirim</th>
                    </tr>
                </thead>
                <tbody>
                </tbody>
            </table>
        </div>
    </div>
</div>
    

@endsection

@section('scripts')
<script src="{{ asset('vendor/datatables/jquery.dataTables.min.js') }}"></script>
<script src="{{ asset('vendor/datatables/datatables.min.js')}}"></script>
<script src="{{ asset('vendor/datatables/datatables.buttons.min.js')}}"></script>
<script src="{{ asset('vendor/datatables/pdfmake.min.js')}}"></script>
<script src="{{ asset('vendor/datatables/vfs_fonts.js')}}"></script>
<script src="{{ asset('vendor/datatables/buttons.html5.min.js')}}"></script>
<script src="{{ asset('vendor/datatables/buttons.print.min.js')}}"></script>
<script src="{{ asset('vendor/datatables/buttons.bootstrap.min.js')}}"></script>
<script src="{{ asset('vendor/datatables/datatables.bootstrap4.min.js')}}"></script>
<script src="{{ asset('vendor/datatables/buttons.flash.min.js')}}"></script>
<script src="{{ asset('vendor/datatables/jszip.min.js')}}"></script>
<script type="text/javascript">
$(document).ready(function() { 

    var start = moment().subtract(29, 'days');
    var end = moment();



    $('#reportrange').daterangepicker({
        startDate: start,
        endDate: end,
        ranges: {
           'Today': [moment(), moment()],
           'Yesterday': [moment().subtract(1, 'days'), moment().subtract(1, 'days')],
           'Last 7 Days': [moment().subtract(6, 'days'), moment()],
           'Last 30 Days': [moment().subtract(29, 'days'), moment()],
           'This Month': [moment().startOf('month'), moment().endOf('month')],
           'Last Month': [moment().subtract(1, 'month').startOf('month'), moment().subtract(1, 'month').endOf('month')]
        }
    }, cb);

  cb(start, end);

    var dt = $('#dataTable').DataTable({
        filter: true,
        processing: true,
        serverSide: true,
        ajax: {
            "url" : '{!! route('log.remin') !!}',
            "type" :"GET",
            "data" :function (d) {
                d._q =$('#_q').val();
                d.startDate = start.format("L");
                d.endDate = end.format("L");
            },
        },
        columns: [
            {data: 'nama_reminder'},
            {data: 'no_wa'},
            {data: 'nama_pasien'},
            {data: 'jenis'},
            {data: 'created_at',sClass: 'text-center'}

        ],
        dom: 'Brt<"bottom"lip><"clear">',
        buttons: [
            { extend : 'excel'},
            { extend : 'pdf'},
            { extend : 'print'}
        ],
    });

    $('#_q').keyup(function(){
        dt.ajax.reload();
    });

    function cb(startDate, endDate) {
        $('#reportrange span').html(start.format('MMMM D, YYYY') + ' - ' + end.format('MMMM D, YYYY'));
        start = startDate;
        end = endDate;
        if(dt)
        dt.ajax.reload();

    }

    

});
</script>

@endsection