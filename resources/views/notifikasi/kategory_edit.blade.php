
@extends('layouts.app')

@section('content')
 	<div class="text-left">
        <h1 class="h4 text-gray-900 mb-4">Edit Kategori Pasien!</h1>
    </div>
    <hr>
    @if ($errors->has('others'))
    <div class="alert alert-danger" role="alert" style="margin-left: 32px;margin-right: 32px;">
		  {{ $errors->first('others') }}
	</div>
	@endif
    <div class="col-md-8">
    	<form action="{{ route('kategory.update',$model->id) }}" method="post">
    		@csrf
    		{{ method_field('PUT') }}
			  <div class="form-group">
			    <label for="exampleInputName1">Nama</label>
			    <input type="text" class="form-control" id="exampleInputName1" aria-describedby="emailHelp" name="name" placeholder="masukan nama kategory" required value="{{ $model->name }}">
			     @if ($errors->has('name'))
			     <small  class="form-text text-danger">{{ $errors->first('name') }}</small>
			     @endif
			  </div>
			  <div class="form-group">
			    <label for="exampleInputEmail1">Keterangan</label>
			    <textarea class="form-control" name="deskription" placeholder="Beri penjelasan kategori ini" rows="3">{{ $model->deskription }}</textarea>
			    @if ($errors->has('deskription'))
			     <small  class="form-text text-danger">{{ $errors->first('deskription') }}</small>
			     @endif
			  </div>
			  
			  <div class="form-group">
			    <label for="exampleInputEmail1">Jenis Pasien</label>
			    <div class="form-group">

			    	<div class="form-check form-check-inline">
						<input class="form-check-input" type="radio" name="jenis" id="inlineRadio1" value="bumil" required @if($model->jenis == 'bumil' ) checked @endif>
					    <label class="form-check-label" for="inlineRadio1">Ibu Hamil</label>
					</div>
					<div class="form-check form-check-inline">
					  <input class="form-check-input" type="radio" name="jenis" id="inlineRadio2" value="bayi" required @if($model->jenis == 'bayi' ) checked @endif>
					  <label class="form-check-label" for="inlineRadio2">Bayi</label>
					</div>
				
			  	</div>
			  </div>
			  <div class="form-group">
			    <label for="exampleInputEmail1">Target Pasien</label>
			    <div class="form-group">

			    	<div class="form-check form-check-inline">
						<input class="form-check-input" type="radio" name="type" id="inlineRadio3" value="Calon Pasien" required @if($model->type == 'Calon Pasien' ) checked @endif >
					    <label class="form-check-label" for="inlineRadio1">Calon Pasien</label>
					</div>
					<div class="form-check form-check-inline">
					  <input class="form-check-input" type="radio" name="type" id="inlineRadio4" value="Pasien" required @if($model->type == 'Pasien' ) checked @endif>
					  <label class="form-check-label" for="inlineRadio2" >Pasien</label>
					</div>
				
			  	</div>
			  </div>
			  <div class="form-group" style="margin-bottom: 50px">
			    <label for="exampleInputEmail1" style="margin-bottom: 30px">Target Usia</label>
			    <br>
			    <input type="hidden" class="range-slider" name="range" value="{{$model->start}},{{$model->end}}" />
			    @if ($errors->has('range'))
			     <small  class="form-text text-danger">{{ $errors->first('range') }}</small>
			     @endif
			  </div>
			  <button type="submit" class="btn btn-primary">Submit</button>
		</form>
    </div>
    
@endsection

@section('scripts')
<script src="{{ asset('vendor/js-range/jquery.range.js') }}"></script>
<script type="text/javascript">
$(document).ready(function(){
	$('.range-slider').jRange({
	    from: 0,
	    to: 100,
	    step: 1,
	    scale: [0,25,50,75,100],
	    format: '%s',
	    width: 600,
	    showLabels: true,
	    isRange : true
	});
});
</script>

@endsection
