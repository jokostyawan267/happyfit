@extends('layouts.app')

@section('content')
 	<div class="text-left">
        <h1 class="h4 text-gray-900 mb-4">Edit Calon Pasien!</h1>
    </div>
    <hr>
    @if ($errors->has('others'))
    <div class="alert alert-danger" role="alert" style="margin-left: 32px;margin-right: 32px;">
		  {{ $errors->first('others') }}
	</div>
	@endif
    <div class="col-md-8">
    	<form action="{{ route('pasien.update',$model->id) }}" method="post" enctype="multipart/form-data">
    		@csrf
    		{{ method_field('PUT') }}
			  <div class="form-group">
			    <label for="exampleInputName1">Nama</label>
			    <input type="text" class="form-control" id="exampleInputName1" aria-describedby="emailHelp" name="name" placeholder="Nama pasien" required value="{{ $model->nama }}">
			     @if ($errors->has('name'))
			     <small  class="form-text text-danger">{{ $errors->first('name') }}</small>
			     @endif
			  </div>
			  <div class="form-group">
			    <label for="exampleInputName1">Nomor WhatsApp</label>
			    <input type="text" class="form-control" id="exampleInputName1" aria-describedby="emailHelp" name="wa" placeholder="08xxx" required value="{{ $model->no_wa }}">
			     @if ($errors->has('name'))
			     <small  class="form-text text-danger">{{ $errors->first('wa') }}</small>
			     @endif
			  </div>
			  
			  <div class="form-group">
			    <label for="exampleInputName1">Usia (Minggu)</label>
			    @if($model->jenis == 'bumil')
			    <input type="number" class="form-control" id="exampleInputName1" aria-describedby="emailHelp" name="usia" placeholder="masukan usia kehamilan/bayi" required  min="1" value="{{ $model->usia_kehamilan_calc }}">
			    @else
			    <input type="number" class="form-control" id="exampleInputName1" aria-describedby="emailHelp" name="usia" placeholder="masukan usia kehamilan/bayi" required  min="1" value="{{ $model->usia_bayi_calc }}">
			    @endif
			     @if ($errors->has('usia'))
			     <small  class="form-text text-danger">{{ $errors->first('usia') }}</small>
			     @endif
			  </div>
			  <div class="form-group">

			    	<div class="form-check form-check-inline">
						<input class="form-check-input" type="radio" name="jenis" id="inlineRadio1" value="bumil" required @if($model->jenis == 'bumil' ) checked @endif>
					    <label class="form-check-label" for="inlineRadio1">Ibu Hamil</label>
					</div>
					<div class="form-check form-check-inline">
					  <input class="form-check-input" type="radio" name="jenis" id="inlineRadio2" value="bayi" required @if($model->jenis == 'bayi' ) checked @endif>
					  <label class="form-check-label" for="inlineRadio2">Bayi</label>
					</div>
				
			  	</div>
			  </div>
			  <button type="submit" class="btn btn-primary">Submit</button>
		</form>
    </div>
    
@endsection

@section('scripts')
<script src="{{ asset('vendor/js-range/jquery.range.js') }}"></script>
<script type="text/javascript">
$(document).ready(function(){
	$('.range-slider').jRange({
	    from: 0,
	    to: 100,
	    step: 1,
	    scale: [0,25,50,75,100],
	    format: '%s',
	    width: 600,
	    showLabels: true,
	    isRange : true
	});
});
</script>

@endsection