 
@extends('layouts.app')

@section('content')


  <!-- Illustrations -->
  <div class="card shadow mb-4">
      <div
          class="card-header py-3 d-flex flex-row align-items-center justify-content-between">
          <h6 class="m-0 font-weight-bold text-primary">Detail Pasien</h6>
          <div class="dropdown no-arrow">
              <a class="dropdown-toggle" href="#" role="button" id="dropdownMenuLink"
                  data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                  <i class="fas fa-ellipsis-v fa-sm fa-fw text-gray-400"></i>
              </a>
              <div class="dropdown-menu dropdown-menu-right shadow animated--fade-in"
                  aria-labelledby="dropdownMenuLink">
                  <div class="dropdown-header">AKSI:</div>
                  @if($model->is_pasien == 'Calon Pasien')
                  <button class="dropdown-item" data-toggle="modal" data-target="#exampleModalCenter"><i class="fas fa-plus"></i> Pasien</button>
                  @else
                  <button class="dropdown-item" data-toggle="modal" data-target="#exampleModalCenter"><i class="fas fa-tasks"></i> Visit</button>
                  @endif
                  <a class="dropdown-item" href="{{ route('pasien.edit',$model->id) }}"><i class="fas fa-edit"></i> Ubah</a>
                  <div class="dropdown-divider"></div>
                  <button class="dropdown-item" id="btn-hapus" style="color: red"><i class="fas fa-trash"></i> Hapus</button>
              </div>
          </div>
      </div>
      <div class="card-body">
          <div class="form-group row">
            <label for="staticEmail" class="col-sm-4 col-form-label font-weight-bold">Nama</label>
            <div class="col-sm-8">
              <label for="staticEmail" class="form-control-plaintext">{{ $model->nama }}</label>
              
            </div>
          </div>
          <div class="form-group row">
            <label for="staticEmail" class="col-sm-4 col-form-label font-weight-bold">Nomor WhatsApp</label>
            <div class="col-sm-8">
               <label for="staticEmail" class="form-control-plaintext">{{ $model->no_wa }}</label>
              
            </div>
          </div>
          <div class="form-group row">
            <label for="staticEmail" class="col-sm-4 col-form-label font-weight-bold">Jenis</label>
            <div class="col-sm-8">
               <label for="staticEmail" class="form-control-plaintext">{{ $model->jenis == 'bumil' ? 'Ibu Hamil' : 'Bayi' }}</label>
             
            </div>
          </div>

          <div class="form-group row">
            <label for="staticEmail" class="col-sm-4 col-form-label font-weight-bold">Status</label>
            <div class="col-sm-8">
               <label for="staticEmail" class="form-control-plaintext">{{ $model->is_pasien }}</label>
            </div>
          </div>
          @if($model->jenis == 'bumil')
          <div class="form-group row">
            <label for="staticEmail" class="col-sm-4 col-form-label font-weight-bold">Usia Awal</label>
            <div class="col-sm-8">
               <label for="staticEmail" class="form-control-plaintext">{{ $model->usia_kehamilan }} minggu</label>
            </div>
          </div>
          <div class="form-group row">
            <label for="staticEmail" class="col-sm-4 col-form-label font-weight-bold">Usia saat ini</label>
            <div class="col-sm-8">
               <label for="staticEmail" class="form-control-plaintext">{{ $model->usia_kehamilan_calc }} minggu</label>
            </div>
          </div>
          @else

          @endif
          @if($model->jenis == 'bumil')
          <div class="form-group row">
            <label for="staticEmail" class="col-sm-4 col-form-label font-weight-bold">Status Persalinan</label>
            <div class="col-sm-8">
               <label for="staticEmail" class="form-control-plaintext">{{ $model->is_persalinan == 1 ? 'Sudah' : 'Belum' }}</label>
            </div>
          </div>
          @endif
          @if($model->is_pasien == 'Pasien')
          <div class="form-group row">
            <label for="staticEmail" class="col-sm-4 col-form-label font-weight-bold">Terakhir Datang</label>
            <div class="col-sm-8">
               <label for="staticEmail" class="form-control-plaintext">{{ $model->last_visit }}</label>
            </div>
          </div>
          @endif
          @if($model->is_pasien == 'Pasien')
          <div class="form-group row">
            <label for="staticEmail" class="col-sm-4 col-form-label font-weight-bold">Catatan</label>
            <div class="col-sm-8">
               <label for="staticEmail" class="form-control-plaintext">{{ $model->note }}</label>
            </div>
          </div>
          @endif
      </div>
  </div>
  <div class="modal fade" id="exampleModalCenter" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
  <div class="modal-dialog modal-dialog-centered" role="document">
    <div class="modal-content">
      <div class="modal-header">
        @if($model->is_pasien == 'Calon Pasien')
        <h5 class="modal-title" id="exampleModalLongTitle"><i class="fas fa-plus"></i> Pasien</h5>
        @else
        <h5 class="modal-title" id="exampleModalLongTitle"><i class="fas fa-tasks"></i> Visit</h5>
        @endif
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <form action="{{ route('pasien.add',$model->id) }}" method="post">
      @csrf
      <div class="modal-body">
        <div class="form-group">
            <label for="exampleInputName1">Catatan Pasien:</label>
            <textarea class="form-control" rows="4" name="note">{{ $model->note }}</textarea>
        </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-secondary" data-dismiss="modal">Tutup</button>
        <button type="submit" class="btn btn-primary">Kirim</button>
      </div>
    </div>
     </form>
  </div>
</div>



@endsection

@section('scripts')
<script type="text/javascript">
$(function(){
    @if(session()->has('status') &&  session()->get('status') == 'S')
    Swal.fire({
      position: 'top-end',
      icon: 'success',
      title: 'Success berhasil melakukan perubahan.',
      showConfirmButton: false,
      timer: 1500
    });
    @endif

    $('#btn-hapus').click(function () {
        var hapus = confirm('apa anda yakin ingin menghapus ini?');
        if(hapus){
            $.ajax({
                url: "{{ url('pasien')}}/{{$model->id}}",
                method: "DELETE",
                data: {
                    _token : "{{ csrf_token() }}"
                },
                datatype: "json",
                async: true,
                success: function (msgd) {
                   Swal.fire({
                    position: 'top-end',
                    icon: 'success',
                    title: msgd.pesan,
                    showConfirmButton: false,
                    timer: 1500
                  });
                   window.location.href="{{ url('pasien') }}";
                },
                error: function (msgd) {
                   
                }

            });
              
        }
      });
});
</script>
@endsection