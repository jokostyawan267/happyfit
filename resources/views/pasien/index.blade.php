 
@extends('layouts.app')

@section('content')
 <!-- Page Heading -->

<!-- DataTales Example -->
    <div class="card shadow mb-4">
    <div class="card-header py-3">
        <h6 class="m-0 font-weight-bold text-primary">Data Pasien</h6>
        <a href="{{ route('pasien.create') }}" style="float: right;" class="btn btn-info"><i class="fas fa-add"></i>Tambah Baru</a>
    </div>
    <div class="card-body">
        <div class="table-responsive">
            <table class="table table-bordered dataTable" id="dataTable" width="100%" cellspacing="0">
                <thead>
                    <tr>
                        <th>Nama</th>
                        <th>No Wa</th>
                        <th>Usia (Minggu)</th>
                        <th>Jenis</th>
                        <th>Status</th>
                        <th>Aksi</th>
                    </tr>
                </thead>
                <tbody>
                   
                </tbody>
            </table>
        </div>
    </div>
</div>
    

@endsection

@section('scripts')

<script src="{{ asset('vendor/datatables/jquery.dataTables.min.js') }}"></script>
<script src="{{ asset('vendor/datatables/dataTables.bootstrap4.min.js') }}"></script>
<script type="text/javascript">
$(document).ready(function() {
  $('#dataTable').DataTable({
        filter: true,
        processing: true,
        serverSide: true,
        ajax: '{!! url('pasien') !!}',
        columns: [
            {data: 'nama'},
            {data: 'no_wa'},
            {
                data: 'jenis',
                "render": function (data, type, full) {
                    if (data == "bumil") {
                         return full.usia_kehamilan_calc;
                    }else{
                         return full.usia_bayi_calc;
                    }
                   
                },
                "className": "text-center",

            },
            {data: 'jenis'},
            {data: 'is_pasien'},
            {data: 'action',className:"text-center"}

        ]
    });
    $('body').on('click', 'tr .btn-hapus', function () {
        var hapus = confirm('apa anda yakin ingin menghapus ini?');
        var id = $(this).data('id');
        if(hapus){
            $.ajax({
                url: "{{ url('pasien')}}/"+id,
                method: "DELETE",
                data: {
                    _token : "{{ csrf_token() }}"
                },
                datatype: "json",
                async: true,
                success: function (msgd) {
                   $("#dataTable").DataTable().ajax.reload();
                },
                error: function (msgd) {
                   
                }

            });
              
        }
      });
});

</script>

@endsection