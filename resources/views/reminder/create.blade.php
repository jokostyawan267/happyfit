
@extends('layouts.app')

@section('content')
 	<div class="text-left">
        <h1 class="h4 text-gray-900 mb-4">Tambah Reminder Baru!</h1>
    </div>
    <hr>
    @if ($errors->has('others'))
    <div class="alert alert-danger" role="alert" style="margin-left: 32px;margin-right: 32px;">
		  {{ $errors->first('others') }}
	</div>
	@endif
    <div class="col-md-8">
    	<form action="{{ route('reminder.store') }}" method="post">
    		@csrf
			  <div class="form-group">
			    <label for="exampleInputName1">Judul</label>
			    <input type="text" class="form-control" id="exampleInputName1" aria-describedby="emailHelp" name="name" placeholder="judul Reminder" required>
			     @if ($errors->has('name'))
			     <small  class="form-text text-danger">{{ $errors->first('name') }}</small>
			     @endif
			  </div>
			  <div class="form-group">
			    <label for="exampleInputEmail1">Deskripsi</label>
			    <textarea class="form-control" name="deskription" placeholder="Isi pesan reminder" rows="3"></textarea>
			    @if ($errors->has('deskription'))
			     <small  class="form-text text-danger">{{ $errors->first('deskription') }}</small>
			     @endif
			  </div>
			  <div class="form-group">
			    <label for="exampleInputName1">Waktu Kirim</label>
			    <input type="number" class="form-control" id="exampleInputName1" aria-describedby="emailHelp" name="jml_hari" placeholder="masukan waktu Kirim dalam satuan hari" required max="30" min="1">
			     @if ($errors->has('name'))
			     <small  class="form-text text-danger">{{ $errors->first('jml_hari') }}</small>
			     @endif
			  </div>
			  <div class="form-group">
			    <label for="exampleInputEmail1">Status Kirim</label>
			    <div class="form-group">
			    	<div class="form-check form-check-inline">
						<input class="form-check-input" type="radio" name="status" id="inlineRadio1" value="1" required>
					    <label class="form-check-label" for="inlineRadio1">Sebelum Kedatangan</label>
					</div>
					<div class="form-check form-check-inline">
					  <input class="form-check-input" type="radio" name="status" id="inlineRadio2" value="2" required>
					  <label class="form-check-label" for="inlineRadio2">Sesudah Kedatangan</label>
					</div>
			  	</div>
			  </div>

			  <div class="form-group">
			    <label for="exampleInputName1">Target Pasien</label>
			    <select class="form-control" name="target_pasien">
			    	<?php foreach ($kategory as $key => $value): ?>
			    		<option value="{{$value->id}}">{{ $value->name }}</option>
			    	<?php endforeach ?>
			    </select>
			</div>
			  <button type="submit" class="btn btn-primary">Submit</button>
		</form>
    </div>
    
@endsection

@section('scripts')
<script src="{{ asset('vendor/js-range/jquery.range.js') }}"></script>
<script type="text/javascript">
$(document).ready(function(){
	$('.range-slider').jRange({
	    from: 0,
	    to: 100,
	    step: 1,
	    scale: [0,25,50,75,100],
	    format: '%s',
	    width: 600,
	    showLabels: true,
	    isRange : true
	});
});
</script>

@endsection
