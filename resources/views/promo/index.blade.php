 
@extends('layouts.app')

@section('content')
 <!-- Page Heading -->

<!-- DataTales Example -->
    <div class="card shadow mb-4">
    <div class="card-header py-3">
        <h6 class="m-0 font-weight-bold text-primary">Promo</h6>
        <a href="{{ route('promo.create') }}" style="float: right;" class="btn btn-info"><i class="fas fa-add"></i>Tambah Baru</a>
    </div>
    <div class="card-body">
        <div class="table-responsive">
            <table class="table table-bordered dataTable table-center" id="dataTable" width="100%" cellspacing="0">
                <thead>
                    <tr>
                        <th>Judul</th>
                        <th>Keterangan</th>
                        <th>Aksi</th>
                    </tr>
                </thead>
                <tbody>
                   
                </tbody>
            </table>
        </div>
    </div>
</div>
    

@endsection

@section('scripts')

<script src="{{ asset('vendor/datatables/jquery.dataTables.min.js') }}"></script>
<script src="{{ asset('vendor/datatables/dataTables.bootstrap4.min.js') }}"></script>
<script type="text/javascript">
$(document).ready(function() {
  $('#dataTable').DataTable({
        filter: true,
        processing: true,
        serverSide: true,
        ajax: {
            "url" : '{!! route('promo.index') !!}',
            "type" :"GET"
        },
        columns: [
            {data: 'judul'},
            {data: 'keterangan'},
            {data: 'action',sClass: 'text-center'}

        ]
    });
    $('body').on('click', 'tr .btn-hapus', function () {
        var hapus = confirm('apa anda yakin ingin menghapus ini?');
        var id = $(this).data('id');
        if(hapus){
            $.ajax({
                url: "{{ url('promo')}}/"+id,
                method: "DELETE",
                data: {
                    _token : "{{ csrf_token() }}"
                },
                datatype: "json",
                async: true,
                success: function (msgd) {
                   $("#dataTable").DataTable().ajax.reload();
                },
                error: function (msgd) {
                   
                }

            });
              
        }
      });
});
</script>

@endsection