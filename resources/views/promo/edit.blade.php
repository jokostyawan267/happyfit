
@extends('layouts.app')

@section('content')
 	<div class="text-left">
        <h1 class="h4 text-gray-900 mb-4">Edit Promo</h1>
    </div>
    <hr>
    @if ($errors->has('others'))
    <div class="alert alert-danger" role="alert" style="margin-left: 32px;margin-right: 32px;">
		  {{ $errors->first('others') }}
	</div>
	@endif
    <div class="col-md-8">
    	<form action="{{ route('promo.update',$model->id) }}" method="post" enctype="multipart/form-data">
    		@csrf
    		{{ method_field('PUT') }}
			  <div class="form-group">
			    <label for="exampleInputName1">Judul</label>
			    <input type="text" class="form-control" id="exampleInputName1" aria-describedby="emailHelp" name="name" placeholder="judul promo" value="{{ $model->judul }}" required>
			     @if ($errors->has('name'))
			     <small  class="form-text text-danger">{{ $errors->first('name') }}</small>
			     @endif
			  </div>
			  <div class="form-group">
			    <label for="exampleInputEmail1">Deskripsi</label>
			    <textarea class="form-control" name="deskription" placeholder="Isi pesan" rows="5">{{ $model->keterangan }}</textarea>
			    @if ($errors->has('deskription'))
			     <small  class="form-text text-danger">{{ $errors->first('deskription') }}</small>
			     @endif
			  </div>
			  <div class="form-group">
			    <label for="exampleInputName1">Gambar</label>
			    <input type="file" name="gambar" id="exampleInputName1" accept="image/*">
			    <img src="{{ url($model->gambar) }}" class="img-view" width="200">
			     @if ($errors->has('name'))
			     <small  class="form-text text-danger">{{ $errors->first('jml_hari') }}</small>
			     @endif
			  </div>
			  <button type="submit" class="btn btn-primary">Submit</button>
		</form>
    </div>
    
@endsection

@section('scripts')
<script src="{{ asset('vendor/js-range/jquery.range.js') }}"></script>
<script type="text/javascript">

$(document).ready(function(){
	function readURLimage(input,targetimage) {
		if (input.files && input.files[0]) {
		  var reader = new FileReader();
		  
		  reader.onload = function(e) {
		    targetimage.attr('src', e.target.result);
		  }
		  
		  reader.readAsDataURL(input.files[0]); // convert to base64 string
		}
	}
	$('.range-slider').jRange({
	    from: 0,
	    to: 100,
	    step: 1,
	    scale: [0,25,50,75,100],
	    format: '%s',
	    width: 600,
	    showLabels: true,
	    isRange : true
	});

	$('input[name=gambar]').change(function () {
          readURLimage(this,$('.img-view'));
    });
});
</script>

@endsection
