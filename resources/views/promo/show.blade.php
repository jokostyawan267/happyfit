 
@extends('layouts.app')

@section('content')
<div class="col-lg-12 mb-4">

    <!-- Illustrations -->
    <div class="card shadow mb-4">
        <div class="card-header py-3">
            <h6 class="m-0 font-weight-bold text-primary">{{$model->judul }}</h6>
        </div>
        <div class="card-body">
            <div class="text-center">
                <img class="img-fluid px-3 px-sm-4 mt-3 mb-4" style="width: 25rem;"
                    src="{{ asset($model->gambar) }}" alt="">
            </div>
            <p>{{ $model->keterangan }}</p>
        </div>
    </div>
    <button class="btn btn-success" data-toggle="modal" data-target="#exampleModalCenter"><i class="fas fa-fw fa-paper-plane"></i> Kirim Promo </button>
</div>
<div class="modal fade" id="exampleModalCenter" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
  <div class="modal-dialog modal-dialog-centered" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="exampleModalLongTitle">Pilih Target Pasien</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <form action="{{ route('promo.send',$model->id) }}" method="post">
      @csrf
      <div class="modal-body">
        <?php foreach ($kategory as $key => $value): ?>
        <div class="form-check">
          <input class="form-check-input" type="checkbox" name="kategory[]" value="{{ $value->id }}">
          <label class="form-check-label" for="defaultCheck1">
            {{ $value->name }} ({{ $value->start }} - {{ $value->end }} minggu)
          </label>
        </div>
        <?php endforeach ?>
        <br>
      <div class="modal-footer">
        <button type="button" class="btn btn-secondary" data-dismiss="modal">Tutup</button>
        <button type="submit" class="btn btn-primary">Kirim</button>
      </div>
    </div>
     </form>
  </div>
</div>


@endsection

@section('scripts')
<script type="text/javascript">
$(function(){
    @if(session()->has('status') &&  session()->get('status') == 'S')
    Swal.fire({
      position: 'top-end',
      icon: 'success',
      title: 'Success promo berhasil dijadwalkan untuk dikirim.',
      showConfirmButton: false,
      timer: 1500
    });
    @endif
});
</script>
@endsection