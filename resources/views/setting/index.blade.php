@extends('layouts.app')

@section('content')
@php
$hpl = \App\ParamVariable::where('var3','max_birth')->first();
@endphp
@if(session()->has('message'))
    <div class="alert alert-success">
        {{ session()->get('message') }}
    </div>
@endif
<div class="row">
    <div class="col-lg-6">
        <div class="card shadow mb-4">
            <div class="card-header py-3">
                <h6 class="m-0 font-weight-bold text-primary">Lama Melahirkan(Minggu)</h6>
            </div>
            <div class="card-body">
                <form class="form-horizontal" action="{{ route('setting.update',$hpl->id) }}" method="POST">
                    @csrf
                    <div class="form-group">
                       
                        <div class="col-lg-12">
                            <label class="control-label">{{__('Perkiraan lahir')}}</label>
                        </div>
                        <div class="col-lg-12">
                            <input type="text" class="form-control" name="var1" value="{{ $hpl->var1 }}"  required>
                        </div>
                    </div>
                    <div class="form-group">
                        <div class="col-lg-12 text-right">
                            <button class="btn btn-info" type="submit">{{__('Save')}}</button>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>

    <div class="col-lg-6">
        <div class="card shadow mb-4">
            <div class="card-header py-3">
                <h6 class="m-0 font-weight-bold text-primary">WOO Key</h6>
            </div>
            <div class="card-body">
                <form class="form-horizontal" action="{{ route('setting.wookey') }}" method="POST">
                    @csrf
                    <div class="form-group">
                       
                        <div class="col-lg-12">
                            <label class="control-label">{{__('Key')}}</label>
                        </div>
                        <div class="col-lg-12">
                            <input type="text" class="form-control" name="var1" value="{{ env('KEY_WOO_API') }}"  required>
                        </div>
                    </div>
                    <div class="form-group">
                        <div class="col-lg-12 text-right">
                            <button class="btn btn-info" type="submit">{{__('Save')}}</button>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>


</div>
@endsection