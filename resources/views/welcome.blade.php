
@extends('layouts.app')

@section('content')

<!-- Content Row -->
<div class="row">

    <!-- Earnings (Monthly) Card Example -->
    <div class="col-xl-3 col-md-6 mb-4">
        <div class="card border-left-primary shadow h-100 py-2">
            <div class="card-body">
                <div class="row no-gutters align-items-center">
                    <div class="col mr-2">
                        <div class="text-xs font-weight-bold text-primary text-uppercase mb-1">
                            Jumlah Pasien Ibu Hamil</div>
                        <div class="h7 mb-0 font-weight-bold text-gray-800">Pasien: <spa id="jml_pasien_bumil">0</spa></div>
                        <div class="h7 mb-0 font-weight-bold text-gray-800">Calon Pasien: <spa id="jml_calon_bumil">0</spa></div>
                    </div>
                    <div class="col-auto">
                        <i class="fas fa-calendar fa-2x text-gray-300"></i>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <!-- Earnings (Monthly) Card Example -->
    <div class="col-xl-3 col-md-6 mb-4">
        <div class="card border-left-success shadow h-100 py-2">
            <div class="card-body">
                <div class="row no-gutters align-items-center">
                    <div class="col mr-2">
                        <div class="text-xs font-weight-bold text-success text-uppercase mb-1">
                            Jumlah Pasien Bayi</div>
                        <div class="h7 mb-0 font-weight-bold text-gray-800">Pasien: <spa id="jml_pasien_bayi">0</spa></div>
                        <div class="h7 mb-0 font-weight-bold text-gray-800">Calon Pasien: <spa id="jml_calon_bayi">0</spa></div>
                    </div>
                    <div class="col-auto">
                        <i class="fas fa-dollar-sign fa-2x text-gray-300"></i>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <!-- Earnings (Monthly) Card Example -->
    <div class="col-xl-3 col-md-6 mb-4">
        <div class="card border-left-info shadow h-100 py-2">
            <div class="card-body">
                <div class="row no-gutters align-items-center">
                    <div class="col mr-2">
                        <div class="text-xs font-weight-bold text-info text-uppercase mb-1">Pasien Ibu hamil hari ini
                        </div>
                        <div class="h7 mb-0 font-weight-bold text-gray-800">Direminder: <spa id="remin_today_bumil">0</spa></div>
                        <div class="h7 mb-0 font-weight-bold text-gray-800">Datang: <spa id="datang_today_bumil">0</spa></div>
                    </div>
                    <div class="col-auto">
                        <i class="fas fa-clipboard-list fa-2x text-gray-300"></i>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <!-- Pending Requests Card Example -->
    <div class="col-xl-3 col-md-6 mb-4">
        <div class="card border-left-warning shadow h-100 py-2">
            <div class="card-body">
                <div class="row no-gutters align-items-center">
                    <div class="col mr-2">
                        <div class="text-xs font-weight-bold text-warning text-uppercase mb-1">
                            Pasien Bayi hari ini</div>
                        <div class="h7 mb-0 font-weight-bold text-gray-800">Direminder: <spa id="remin_today_bayi">0</spa></div>
                        <div class="h7 mb-0 font-weight-bold text-gray-800">Datang: <spa id="datang_today_bayi">0</spa></div>
                    </div>
                    <div class="col-auto">
                        <i class="fas fa-comments fa-2x text-gray-300"></i>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<?php $years = range(2020, strftime("%Y", time())); ?>
<div class="row">

    <!-- Area Chart -->
    <div class="col-xl-8 col-lg-7">
        <div class="card shadow mb-4">
            <!-- Card Header - Dropdown -->
            <div
                class="card-header py-3 d-flex flex-row align-items-center justify-content-between">
                <h6 class="m-0 font-weight-bold text-primary">Pasien Datang</h6>
                <div class="col-md-6">
                    <div style="float: right;">
                        <select class="form-control" id="select_tahun">
                          <?php foreach($years as $year) : ?>
                            <option  value="<?php echo $year; ?>" @if($year == date('Y')) selected @endif ><?php echo $year; ?></option>
                          <?php endforeach; ?>
                        </select>
                    </div>
                </div>
                
            </div>
            <!-- Card Body -->
            <div class="card-body">
                <div class="chart-area">
                    <canvas id="myAreaChart"></canvas>
                </div>
            </div>
        </div>
    </div>
    
    

    <!-- Pie Chart -->
    <div class="col-xl-4 col-lg-5">
        <div class="card shadow mb-4">
            <!-- Card Header - Dropdown -->
            <div
                class="card-header py-3 d-flex flex-row align-items-center justify-content-between">
                <h6 class="m-0 font-weight-bold text-primary">Promo </h6>
                <div style="float: right;">
                    <a href="#" class="btn small dropdown-item" id="datepicker" data-date-format="mm-yyyy" data-date="{{date('m-Y')}}">{{date('F Y')}}</a>
                </div>
                
            </div>
            <!-- Card Body -->
            <div class="card-body">
                <div class="chart-pie pt-4 pb-2">
                    <canvas id="myPieChart"></canvas>
                </div>
                <div class="mt-4 text-center small">
                    <span class="mr-2">
                        <i class="fas fa-circle text-primary"></i> Promo Ibu hamil
                    </span>
                    <span class="mr-2">
                        <i class="fas fa-circle text-success"></i> Promo bayi
                    </span>
                    <span class="mr-2">
                        <i class="fas fa-circle text-info"></i> Pasien ibu hamil datang
                    </span>
                    <span class="mr-2">
                        <i class="fas fa-circle text-danger"></i> Pasien bayi datang
                    </span>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection

@section('scripts')
<script src="{{ asset('vendor/chart.js/Chart.min.js') }}"></script>
<!-- Page level custom scripts -->
<script src="{{ asset('vendor/datepicker/js/bootstrap-datepicker.js') }}"></script>
<script type="text/javascript">
const monthNames = ["January", "February", "March", "April", "May", "June",
        "July", "August", "September", "October", "November", "December"];


$(function(){
    Chart.defaults.global.defaultFontFamily = 'Nunito', '-apple-system,system-ui,BlinkMacSystemFont,"Segoe UI",Roboto,"Helvetica Neue",Arial,sans-serif';
    Chart.defaults.global.defaultFontColor = '#858796';
    // Pie Chart Example
    var ctx = document.getElementById("myPieChart");

    function getPromo(xdate){
    $.ajax({
        url: "{{ url('dashboard/promo-bulanan')}}",
        method: "GET",
        datatype: "json",
        data:{
            "bulan" : xdate
        },
        async: true,
        success: function (msgd) {
           console.log(msgd);
           $("promo-bulan").html(msgd.bulan_d);

                var promoBumil = msgd.promo  != null ? msgd.promo.jml_bumil:0;
                var promoBayi = msgd.promo  != null ? msgd.promo.jml_bayi:0;

                var datangBumil =  msgd.datang != null ? msgd.datang.jml_bumil:0;
                var datangBayi =  msgd.datang != null? msgd.datang.jml_bayi:0;
                if(promoBumil> 0 || promoBayi>0 || datangBumil>0 || datangBayi>0){
                    $("#myPieChart").show();
                    var myPieChart = new Chart(ctx, {
                      type: 'doughnut',
                      data: {
                        labels: ["Promo Ibu hamil", "Promo bayi", "Pasien Ibu hamil datang","Pasien Bayi datang"],
                        datasets: [{
                          data: [promoBumil, promoBayi, datangBumil,datangBayi],
                          backgroundColor: ['#4e73df', '#1cc88a', '#36b9cc','#de2849'],
                          hoverBackgroundColor: ['#2e59d9', '#17a673', '#2c9faf','#f20c36'],
                          hoverBorderColor: "rgba(234, 236, 244, 1)",
                        }],
                      },
                      options: {
                        maintainAspectRatio: false,
                        tooltips: {
                          backgroundColor: "rgb(255,255,255)",
                          bodyFontColor: "#858796",
                          borderColor: '#dddfeb',
                          borderWidth: 1,
                          xPadding: 15,
                          yPadding: 15,
                          displayColors: false,
                          caretPadding: 10,
                        },
                        legend: {
                          display: false
                        },
                        cutoutPercentage: 80,
                      },
                    });
                }else{
                    $("#myPieChart").hide();
                }
                
        },
        error: function (msgd) {
           
        }

    });
}
    
    $.ajax({
        url: "{{ url('dashboard')}}",
        method: "POST",
        datatype: "json",
        data:{
            _token : "{{ csrf_token() }}"
        },
        async: true,
        success: function (msgd) {
           console.log(msgd);
           $("#jml_pasien_bayi").html(msgd.pasien.bayi);
           $("#jml_pasien_bumil").html(msgd.pasien.bumil);
           $("#jml_calon_bayi").html(msgd.pasien.calon_pasien_bayi);
           $("#jml_calon_bumil").html(msgd.pasien.calon_pasien_bumil);
           $("#datang_today_bayi").html(msgd.datang_today.jml_bayi);
           $("#datang_today_bumil").html(msgd.datang_today.jml_bumil);
           $("#remin_today_bayi").html(msgd.remin_today.jml_bayi);
           $("#remin_today_bumil").html(msgd.remin_today.jml_bumil);
        },
        error: function (msgd) {
           
        }

    });

    getPromo("{{ date('m-Y') }}");

    

    $("#datepicker").datepicker( {
        format: "mm-yyyy",
        viewMode: "months", 
        minViewMode: "months",
        autoclose:true
    }).on('changeDate', function(ev){
        $('.datepicker').hide();
        const dateObj = new Date(ev.date);
        const month = monthNames[dateObj.getMonth()];
        const year = dateObj.getFullYear();
        $(this).html(month+" "+year);
        var month_name = dateObj.getMonth()+1;
        if(month_name<10){
            month_name = '0'+(month_name);
        }
        getPromo(month_name+"-"+year);
        console.log(month_name+"-"+year);
    });




    function number_format(number, decimals, dec_point, thousands_sep) {
      // *     example: number_format(1234.56, 2, ',', ' ');
      // *     return: '1 234,56'
      number = (number + '').replace(',', '').replace(' ', '');
      var n = !isFinite(+number) ? 0 : +number,
        prec = !isFinite(+decimals) ? 0 : Math.abs(decimals),
        sep = (typeof thousands_sep === 'undefined') ? ',' : thousands_sep,
        dec = (typeof dec_point === 'undefined') ? '.' : dec_point,
        s = '',
        toFixedFix = function(n, prec) {
          var k = Math.pow(10, prec);
          return '' + Math.round(n * k) / k;
        };
      // Fix for IE parseFloat(0.55).toFixed(0) = 0;
      s = (prec ? toFixedFix(n, prec) : '' + Math.round(n)).split('.');
      if (s[0].length > 3) {
        s[0] = s[0].replace(/\B(?=(?:\d{3})+(?!\d))/g, sep);
      }
      if ((s[1] || '').length < prec) {
        s[1] = s[1] || '';
        s[1] += new Array(prec - s[1].length + 1).join('0');
      }
      return s.join(dec);
    }

    function getDataArea(year){

        $.ajax({
            url: "{{ url('dashboard/kedatangan')}}",
            method: "GET",
            datatype: "json",
            data:{
                tahun:year
            },
            async: true,
            success: function (msgd) {
               console.log(msgd);
               if(msgd.value.length > 0)
               $("#myAreaChart").show();
               else
               $("#myAreaChart").hide();
               var ctx = document.getElementById("myAreaChart");
               var myLineChart = new Chart(ctx, {
                  type: 'line',
                  data: {
                    labels: msgd.bulan,
                    datasets: [{
                      label: "Jumlah :",
                      lineTension: 0.3,
                      backgroundColor: "rgba(78, 115, 223, 0.05)",
                      borderColor: "rgba(78, 115, 223, 1)",
                      pointRadius: 3,
                      pointBackgroundColor: "rgba(78, 115, 223, 1)",
                      pointBorderColor: "rgba(78, 115, 223, 1)",
                      pointHoverRadius: 3,
                      pointHoverBackgroundColor: "rgba(78, 115, 223, 1)",
                      pointHoverBorderColor: "rgba(78, 115, 223, 1)",
                      pointHitRadius: 10,
                      pointBorderWidth: 2,
                      data: msgd.value,
                    }],
                  },
                  options: {
                    maintainAspectRatio: false,
                    layout: {
                      padding: {
                        left: 10,
                        right: 25,
                        top: 25,
                        bottom: 0
                      }
                    },
                    scales: {
                      xAxes: [{
                        time: {
                          unit: 'date'
                        },
                        gridLines: {
                          display: false,
                          drawBorder: false
                        },
                        ticks: {
                          maxTicksLimit: 7
                        }
                      }],
                      yAxes: [{
                        ticks: {
                          maxTicksLimit: 5,
                          padding: 10,
                          // Include a dollar sign in the ticks
                          callback: function(value, index, values) {
                            return number_format(value);
                          }
                        },
                        gridLines: {
                          color: "rgb(234, 236, 244)",
                          zeroLineColor: "rgb(234, 236, 244)",
                          drawBorder: false,
                          borderDash: [2],
                          zeroLineBorderDash: [2]
                        }
                      }],
                    },
                    legend: {
                      display: false
                    },
                    tooltips: {
                      backgroundColor: "rgb(255,255,255)",
                      bodyFontColor: "#858796",
                      titleMarginBottom: 10,
                      titleFontColor: '#6e707e',
                      titleFontSize: 14,
                      borderColor: '#dddfeb',
                      borderWidth: 1,
                      xPadding: 15,
                      yPadding: 15,
                      displayColors: false,
                      intersect: false,
                      mode: 'index',
                      caretPadding: 10,
                      callbacks: {
                        label: function(tooltipItem, chart) {
                          var datasetLabel = chart.datasets[tooltipItem.datasetIndex].label || '';
                          return datasetLabel + number_format(tooltipItem.yLabel);
                        }
                      }
                    }
                  }
                });
            },
            error: function (msgd) {
               
            }

        });

    }

    getDataArea("{{ date('Y') }}");

    $('#select_tahun').change(function(){
        var yr = $(this).val();
        getDataArea(yr);
    });




});
</script>

@endsection