<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class LogRemin extends Model
{
    //
    protected $table = 'log_reminder';
    protected $primaryKey = 'id';
}
