<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class KategoryPasien extends Model
{
    //
    protected $table = 'kategory_pasien';
    protected $primaryKey = 'id';
}
