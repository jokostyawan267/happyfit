<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ProsesPasien extends Model
{
    //
    protected $table = 'pros_pasien';
    protected $primaryKey = 'id';
}
