<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use App\LogPromo;
use Yajra\DataTables\Facades\DataTables;
use Carbon\Carbon;

class LaporanController extends Controller
{
    //

    public function logPromo(Request $request)
    {
    	if($request->ajax()){
            // dd($request->all());
            
            $model = \DB::table("log_promo");
            
            if($request->has('startDate') && $request->has('endDate')) {

                $start = Carbon::createFromFormat('m/d/Y H:i:s',  $request->startDate." 00:00:00");
                $end = Carbon::createFromFormat('m/d/Y H:i:s',  $request->endDate." 00:00:00");
                $model->where('created_at', ">=", $start)->where('created_at', "<=", $end);
            }
    		if(strlen($request->_q)>0){
                $model->whereRaw("UPPER(log) LIKE '%". strtoupper($request->_q)."%'")->orWhereRaw("UPPER(`to`) LIKE '%". strtoupper($request->_q)."%'");
            }

    		$datatables = Datatables::of($model)->addColumn('judul', function ($model) {
    			$log = json_decode($model->log);
    			if(strlen($log->judul) > 25){
    				return substr($log->judul, 0,25)."...." ; 
    			}
    			return $log->judul;

            })->addColumn('no_wa', function ($model) {
                return json_decode($model->to)->no_wa; 
            })->addColumn('nama', function ($model) {
                return json_decode($model->to)->nama; 
            })->addColumn('jenis', function ($model) {
                return json_decode($model->to)->jenis; 
            });
    		return $datatables->make(true);
    	}
    	return view("laporan.logPromo");

    }

    public function logReminder(Request $request)
    {
        if($request->ajax()){
            // dd($request->all());
            
            $model = \DB::table("log_reminder")
            ->select(['pros_pasien.nama as nama_pasien','pros_pasien.jenis','pros_pasien.no_wa','reminder.nama as nama_reminder','log_reminder.created_at'])
            ->join('pros_pasien', 'pros_pasien.id', '=', 'log_reminder.pasien_id')
            ->join('reminder', 'reminder.id', '=', 'log_reminder.reminder_id');

            if($request->has('startDate') && $request->has('endDate')) {
                $start = Carbon::createFromFormat('m/d/Y H:i:s',  $request->startDate." 00:00:00");
                $end = Carbon::createFromFormat('m/d/Y H:i:s',  $request->endDate." 00:00:00");
                $model->where('log_reminder.created_at', ">=", $start)->where('log_reminder.created_at', "<=", $end);
            }
            if(strlen($request->_q)>0){
                $model->whereRaw("UPPER(pros_pasien.nama) LIKE '%". strtoupper($request->_q)."%'")->orWhereRaw("UPPER(pros_pasien.no_wa) LIKE '%". strtoupper($request->_q)."%'")->
                    orWhereRaw("UPPER(reminder.nama) LIKE '%". strtoupper($request->_q)."%'");
            }

            $datatables = Datatables::of($model);
            return $datatables->make(true);
        }
        return view("laporan.reminderlog");

    }

    
}
