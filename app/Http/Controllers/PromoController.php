<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Yajra\DataTables\Facades\DataTables;
use App\NotifPasien;
use App\KategoryPasien;
use Response;
use Auth;
use App\Http\Controllers\Api;
use App\LogPromo;
use App\JobPromo;

class PromoController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        //
        if($request->ajax()){
            $model = NotifPasien::where('is_deleted','0');
            $datatables = Datatables::of($model)
            ->addColumn('action', function ($model) {
                return '
                        <a href="'.route('promo.show',$model->id).'" class="btn btn-primary btn-circle btn-sm">
                            <i class="fas fa-eye"></i>
                        </a>&nbsp;<a href="'.route('promo.edit',$model->id).'" class="btn btn-primary btn-circle btn-sm">
                            <i class="fas fa-edit"></i>
                        </a>&nbsp;
                        <button class="btn btn-danger btn-circle btn-sm btn-hapus" data-id="'.$model->id.'">
                            <i class="fas fa-trash"></i>
                        </button>'; 
            });
            return $datatables->make(true);
        }

        return view('promo.index');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
        return view('promo.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
        // dd($request->all());
        
        $model = new NotifPasien;
        $model->judul = $request->name;
        $model->keterangan = $request->deskription;
        $model->created_by = Auth::user()->id;
        $model->updated_by = Auth::user()->id;
        if($request->has('gambar')){
         
            $path = public_path();
            $host = $_SERVER['SERVER_NAME'];
            $destination_dir = $path."/uploads/promo/";
            $base_filename = basename($_FILES["gambar"]["name"]);
            $temp = explode(".", $_FILES["gambar"]["name"]);
            $newfilename = round(microtime(true)) . '.' . end($temp);
            $target_file = $destination_dir . $newfilename;
            if(move_uploaded_file($_FILES["gambar"]["tmp_name"], $target_file)){
                $model->gambar = "uploads/promo/".$newfilename;
            }
         
        }
        if($model->save()){
            return \Redirect::to('/promo');
        }
        return redirect()->back()->withErrors([
            'others' => "Something went wrong.",
        ]);

    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
        $kategory = KategoryPasien::where('is_deleted','0')->get();
        $model = NotifPasien::findOrFail($id);
        return view('promo.show',compact('model','kategory'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
        $model = NotifPasien::findOrFail($id);
        return view('promo.edit',compact('model'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
        // dd($request->all());
        $model = NotifPasien::findOrFail($id);
        $model->judul = $request->name;
        $model->keterangan = $request->deskription;
        $model->updated_by = Auth::user()->id;
        if($request->has('gambar')){
         
            $path = public_path();
            $host = $_SERVER['SERVER_NAME'];
            $destination_dir = $path."/uploads/promo/";
            $base_filename = basename($_FILES["gambar"]["name"]);
            $temp = explode(".", $_FILES["gambar"]["name"]);
            $newfilename = round(microtime(true)) . '.' . end($temp);
            $target_file = $destination_dir . $newfilename;
            if(move_uploaded_file($_FILES["gambar"]["tmp_name"], $target_file)){
                $model->gambar = "uploads/promo/".$newfilename;
            }
         
        }
        if($model->save()){
            return \Redirect::to('/promo');
        }
        return redirect()->back()->withErrors([
            'others' => "Something went wrong.",
        ]);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
        try {
            $model = NotifPasien::findOrFail($id);
            $model->is_deleted = 1;
            if($model->save()){
                return Response::json(['status' => 'S','pesan' => 'Berhasil menghapus data.']);
            }
            return Response::json(['status' => 'E','pesan' => 'Gagal menghapus data.']);
        } catch (Exception $e) {
            return Response::json(['status' => 'E','pesan' => 'Gagal menghapus data.']);
        }
    }


    public function upload(Request $request)
    {
        // dd($file);

        $destination_dir = "../../public/uploads/promo/";
        $base_filename = basename($_FILES["file"]["name"]);
        $temp = explode(".", $_FILES["file"]["name"]);
        $newfilename = round(microtime(true)) . '.' . end($temp);
        $target_file = $destination_dir . $newfilename;

        if(!$_FILES["file"]["error"])
        {
           
            if (move_uploaded_file($_FILES["file"]["tmp_name"], $target_file)) {        
                $response->status = true;
                $response->message = "File uploaded successfully";
                $response->path = 'uploads/promo/'.$newfilename;

            } else {

                $response->status = false;
                $response->message = "File uploading failed";
            }    
        } 
        else
        {
            $response->status = false;
            $response->message = $_FILES["file"]["error"];
        }

    }

    public function kirimPromo(Request $request,$id)
    {
       try {
           foreach ($request->kategory as $key => $value) {
                # code...
                $model = new JobPromo;
                $model->kategori_id = $value;
                $model->promo_id = $id;
                $model->save();
            }
            return redirect()->back()->with('status', 'S');
       } catch (Exception $e) {
           
       }
        
    }

}
