<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\User;
use App\UserRole;


class AdminController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
        $data = User::where('is_deleted',0)->orderBy('id','desc')->get();
        return view('user-admin.index',compact('data'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
        return view('user-admin.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
       $payload = $request->validate([
            'name' => 'required|min:2',
            'email' => 'required|email|unique:users',
            'password' => 'required|min:8',
        ]);

        $payload['password'] = \Hash::make($payload['password']);
        $user = User::create($payload);
        if(isset($user)){
            $role = new UserRole;
            $role->role = $request->role;
            $role->user_id = $user->id;
            if($role->save()){
                return \Redirect::to('/user-admin');
            }
        }

        return redirect()->back()->withErrors([
            'others' => "Something went wrong.",
        ]);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
        $data = User::findOrfail($id);
        return view('user-admin.edit',compact('data')); 

    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
        $request->validate([
            'name' => 'required|min:2',
        ]);

        $data = User::findOrfail($id);
        if($request->has('password') && $request->password != null )$data->password = \Hash::make($request->password);
        $data->name = $request->name;
        if($data->save()){
            $role = UserRole::where('user_id',$id)->first();
            $role->role = $request->role;
            if($role->save()){
                if($request->role == 'user' &&  auth()->user()->id == $id)
                    return \Redirect::to('/');
                return \Redirect::to('/user-admin');
            }
        }
        return redirect()->back()->withErrors([
            'others' => "Something went wrong.",
        ]);

        
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
         $data = User::findOrfail($id);
         $data->is_deleted =1;
         $data->save();
         if(auth()->user()->id == $id)
            return \Redirect::to('/');
         return \Redirect::to('/user-admin');

    }
}
