<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Yajra\DataTables\Facades\DataTables;
use App\Reminder;
use App\KategoryPasien;
use Response;

class ReminderController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        //
        if($request->ajax()){
            $model = Reminder::where('is_deleted','0')->with('kategory');
            $datatables = Datatables::of($model)
            ->addColumn('action', function ($model) {
                return '<a href="'.route('reminder.edit',$model->id).'" class="btn btn-primary btn-circle btn-sm">
                            <i class="fas fa-edit"></i>
                        </a>&nbsp;
                        <button class="btn btn-danger btn-circle btn-sm btn-hapus" data-id="'.$model->id.'">
                            <i class="fas fa-trash"></i>
                        </button>'; 
            });
            return $datatables->make(true);

        }
        return view('reminder.index');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
        $kategory = KategoryPasien::where('is_deleted','0')->get();
        return view('reminder.create',compact('kategory'));
    }
    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
        $data = new  Reminder();
        $data->nama = $request->name; 
        $data->keterangan = $request->deskription;
        $data->status_kirim = $request->status;
        $data->kategori_id = $request->target_pasien;
        $data->jumlah_hari = $request->jml_hari; 
        if($data->save()){
             return \Redirect::to('/reminder');
        }
        return redirect()->back()->withErrors([
            'others' => "Something went wrong.",
        ]);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
        $model = Reminder::findOrFail($id);
        $kategory = KategoryPasien::where('is_deleted','0')->get();
        return view('reminder.edit',compact('model','kategory'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //

        try {
            $model = Reminder::findOrFail($id);
            $model->nama = $request->name; 
            $model->keterangan = $request->deskription;
            $model->status_kirim = $request->status;
            $model->kategori_id = $request->target_pasien;
            $model->jumlah_hari = $request->jml_hari; 
            if($model->save()){
                 return \Redirect::to('/reminder');
            }

            return redirect()->back()->withErrors([
                'others' => "Something went wrong.",
            ]);
        } catch (Exception $e) {
           return redirect()->back()->withErrors([
                'others' => "Something went wrong.",
            ]);
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
        try {
            $model = Reminder::findOrFail($id);
            $model->is_deleted = 1;
            if($model->save()){
                return Response::json(['status' => 'S','pesan' => 'Berhasil menghapus data.']);
            }
            return Response::json(['status' => 'E','pesan' => 'Gagal menghapus data.']);
        } catch (Exception $e) {
            return Response::json(['status' => 'E','pesan' => 'Gagal menghapus data.']);
        }
    }
}
