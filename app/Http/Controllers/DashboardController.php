<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Auth;
use App\VwDashboardPasien;
use Response;
use App\VwDashboardReminToday;
use App\VwDashboardDatangToday;
use App\VwDashboardPromoBulan;
use App\VwDashboardDatangBulan;
use DB;

class DashboardController extends Controller
{
    //
     /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index(Request $request)
    {
        if ($request->ajax() && $request->method() == 'POST') {
            # code...
            $data['pasien'] = VwDashboardPasien::first();
            $data['remin_today'] = VwDashboardReminToday::first();
            $data['datang_today'] = VwDashboardDatangToday::first();
            return Response::json($data);
        }
    	
        return view('welcome');
    }

    public function login_view(){
        return view('login');
    }

    public function login_web(Request $request){

        if(Auth::guard('web')->attempt(['email'=>$request['email'],'password'=>$request['password'],'is_deleted' => 0])) {
            return \Redirect::to('/dashboard');
        }
       
        return redirect()->back()->withErrors([
	                'others' => "Kombinasi email dan password salah.",
	            ]);
    }

    public function logout()
	{
	    Auth::logout();
	    return \Redirect::to('/');
	}

    public function promoKedatangan(Request $request)
    {
        if($request->ajax())
        {
            $bulan = date('m-Y');

            if($request->has('bulan')){
                $bulan = $request->bulan;
            }

            $bulan_d = date('M Y');
            $promo = VwDashboardPromoBulan::where('bulan_tahun',$bulan)->first();
            $datang = VwDashboardDatangBulan::where('bulan_tahun',$bulan)->first();
            return Response::json(compact('bulan_d','promo','datang'));

        }

    }

    public function kedatangan(Request $request)
    {
        $model = \DB::table("vw_datang_bulanan")
        ->where('tahun',$request->tahun)
        ->orderBy('bulan_tahun','asc')->get();
        $collection = collect($model);
        $bulan = $collection->pluck('bulan')->all();
        $value = $collection->pluck('jml')->all();
        return \Response::json(compact('bulan','value'));


    }
}
