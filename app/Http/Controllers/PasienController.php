<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\ProsesPasien;
use Yajra\DataTables\Facades\DataTables;
use App\ParamVariable;
use Carbon\Carbon;
use Auth;
use DB;
use Response;
use App\Kedatangan;

class PasienController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        //

        if($request->ajax()){
            $model = ProsesPasien::where('is_deleted',0);
            $datatables = Datatables::of($model)
            ->addColumn('action', function ($model) {
                return '<a href="'.route('pasien.show',$model->id).'" class="btn btn-primary btn-circle btn-sm">
                            <i   class="fas fa-eye"></i>
                        </a>&nbsp;<a href="'.route('pasien.edit',$model->id).'" class="btn btn-primary btn-circle btn-sm">
                            <i class="fas fa-edit"></i>
                        </a>
                        <button class="btn btn-danger btn-circle btn-sm btn-hapus" data-id="'.$model->id.'">
                            <i class="fas fa-trash"></i>
                        </button>'; 
            });
            return $datatables->make(true);

        }
        return view('pasien.index');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //

        return view('pasien.create');

    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
        // dd($request->all());
        $model = new ProsesPasien();
        $model->nama = $request->name;
        $model->no_wa = $request->wa;
        $model->jenis = $request->jenis;

        if($model->jenis == 'bumil')
        {
            $param = ParamVariable::where('var3','max_birth')->first();
            $minggu = $param->var1 - $request->usia;
            $hari = Carbon::now()->addDays($minggu*7);
            $model->usia_kehamilan =  $request->usia;
            $model->usia_kehamilan_calc =  $request->usia;
            $model->hpl = $hari;
        }else{
            $model->usia_bayi =  $request->usia;
            $model->usia_bayi_calc =  $request->usia;
        }

        $model->tgl_daftar = Carbon::now();
        $model->created_by = Auth::user()->id;
        $model->updated_by = Auth::user()->id;
        $model->save();

        if($model->save()){
            return \Redirect::to('/pasien');
        }
        return redirect()->back()->withErrors([
            'others' => "Something went wrong.",
        ]);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
        $model = ProsesPasien::findOrFail($id);
        return view('pasien.show',compact('model'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
        $model = ProsesPasien::findOrFail($id);
        return view('pasien.edit',compact('model'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
        $model = ProsesPasien::findOrFail($id);
        $model->nama = $request->name;
        $model->no_wa = $request->wa;
        $model->jenis = $request->jenis;

        if($model->jenis == 'bumil')
        {
            $param = ParamVariable::where('var3','max_birth')->first();
            $minggu = $param->var1 - $request->usia;
            $hari = Carbon::now()->addDays($minggu*7);
            $model->usia_kehamilan =  $request->usia;
            $model->usia_kehamilan_calc =  $request->usia;
            $model->hpl = $hari;
        }else{
            $model->usia_bayi =  $request->usia;
            $model->usia_bayi_calc =  $request->usia;
        }
        $model->updated_by = Auth::user()->id;
        $model->save();

        if($model->save()){
            return \Redirect::to('/pasien');
        }
        return redirect()->back()->withErrors([
            'others' => "Something went wrong.",
        ]);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
        try {
            $model = ProsesPasien::findOrFail($id);
            $model->is_deleted = 1;
            if($model->save()){
                return Response::json(['status' => 'S','pesan' => 'Berhasil menghapus data.']);
            }
            return Response::json(['status' => 'E','pesan' => 'Gagal menghapus data.']);
        } catch (Exception $e) {
            return Response::json(['status' => 'E','pesan' => 'Gagal menghapus data.']);
        }
    }

    public function jadikanPasien(Request $request,$id)
    {
        try {
            $model = ProsesPasien::findOrFail($id);
            $model->last_visit = Carbon::now();
            $model->note = $request->note;
            $model->is_pasien = 'Pasien';
            $model->updated_by = Auth::user()->id;
            if($model->save()){
                $kedatangan = new Kedatangan;
                $kedatangan->tgl_datang = Carbon::now();
                $kedatangan->id_pasien = $model->id;
                $kedatangan->is_arrival = 1;
                $kedatangan->save();
                return redirect()->back()->with('status', 'S');
            }
            return redirect()->back()->with('status', 'E');
            
        } catch (Exception $e) {
            return redirect()->back()->with('status', 'E');
        }

    }
}
