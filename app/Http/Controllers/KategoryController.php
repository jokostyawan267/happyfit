<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\KategoryPasien;
use Response;
use Yajra\DataTables\Facades\DataTables;

class KategoryController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        //
        if($request->ajax()){

            $model = KategoryPasien::where('is_deleted','0');
            $datatables = Datatables::of($model)
            ->addColumn('action', function ($model) {
                return '<a href="'.route('kategory.edit',$model->id).'" class="btn btn-primary btn-circle btn-sm">
                            <i class="fas fa-edit"></i>
                        </a>&nbsp;
                        <button class="btn btn-danger btn-circle btn-sm btn-hapus" data-id="'.$model->id.'">
                            <i class="fas fa-trash"></i>
                        </button>'; 
            });
            return $datatables->make(true);
        }
        return view('notifikasi.kategory');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
        return view('notifikasi.kategory_create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
        // dd($request->all());
        $range = explode(",", $request->range);
        $data = new KategoryPasien();
        $data->name = $request->name; 
        $data->deskription = $request->deskription;
        $data->start = $range[0];
        $data->end = $range[1];
        $data->jenis = $request->jenis;
        if($data->save()){
             return \Redirect::to('/kategory');
        }
        return redirect()->back()->withErrors([
            'others' => "Something went wrong.",
        ]);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
        $model = KategoryPasien::findOrFail($id);
        return view('notifikasi.kategory_edit',compact('model'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
        try {
            $range = explode(",", $request->range);
            $model = KategoryPasien::findOrFail($id);
            $model->name = $request->name; 
            $model->deskription = $request->deskription;
            $model->start = $range[0];
            $model->end = $range[1];
            $model->jenis = $request->jenis;
            if($model->save()){
                 return \Redirect::to('/kategory');
            }

            return redirect()->back()->withErrors([
                'others' => "Something went wrong.",
            ]);
        } catch (Exception $e) {
           return redirect()->back()->withErrors([
                'others' => "Something went wrong.",
            ]);
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
        try {
            $model = KategoryPasien::findOrFail($id);
            $model->is_deleted = 1;
            if($model->save()){
                return Response::json(['status' => 'S','pesan' => 'Berhasil menghapus data.']);
            }
            return Response::json(['status' => 'E','pesan' => 'Gagal menghapus data.']);
        } catch (Exception $e) {
            return Response::json(['status' => 'E','pesan' => 'Gagal menghapus data.']);
        }
    }
}
