<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\ProsesPasien;
use App\ParamVariable;
use App\Kedatangan;
use Carbon\Carbon;
use Auth;

class PasienController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        //
        $pasien = ProsesPasien::where(['is_deleted' => 0,'is_pasien' => 'Pasien']);

        if ($request->has('jenis') && $request->jenis != "all") {
            # code...
            $pasien = $pasien->where('jenis', $request->jenis);
        }
        if($request->_q != '')
        {
            $q = $request->_q;
            # code...
            $pasien =$pasien->whereRaw("upper(nama) LIKE '%".strtoupper($q)."%'")
            ->orWhereRaw("upper(no_wa) LIKE '%".strtoupper($q)."%'")
            ->orWhereRaw("upper(usia_kehamilan) LIKE '%".strtoupper($q)."%'");
        }
        $pasien = $pasien->orderBy("created_at","desc")->paginate(7);
        return response()->json(compact('pasien'), 200);
    }

    public function calon(Request $request)
    {
        //
        $pasien = ProsesPasien::where(['is_deleted' => 0,'is_pasien' => 'Calon Pasien']);

        if ($request->has('jenis') && $request->jenis != "all") {
            # code...
            $pasien = $pasien->where('jenis', $request->jenis);
        }
        if($request->_q != '')
        {
            $q = $request->_q;
            # code...
            $pasien =$pasien->whereRaw("upper(nama) LIKE '%".strtoupper($q)."%'")
            ->orWhereRaw("upper(no_wa) LIKE '%".strtoupper($q)."%'")
            ->orWhereRaw("upper(usia_kehamilan) LIKE '%".strtoupper($q)."%'");
        }
        $pasien = $pasien->orderBy("created_at","desc")->paginate(7);
        return response()->json(compact('pasien'), 200);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create(Request $request)
    {
        //
        $model = ProsesPasien::findOrFail($request->id);
        $model->last_visit = Carbon::now();
        $model->note = $request->note;
        $model->is_pasien = 'Pasien';
        $model->updated_by = Auth::user()->id;
        if($model->save()){
            $kedatangan = new Kedatangan;
            $kedatangan->tgl_datang = Carbon::now();
            $kedatangan->id_pasien = $model->id;
            $kedatangan->is_arrival = 1;
            $kedatangan->save();
            return response()->json(compact('model'), 200);
        }
        return response()->json(compact('model'), 500);

    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
        try {
            $model = new ProsesPasien();
            $model->nama = $request->nama;
            $model->no_wa = $request->no_wa;
            $model->jenis = $request->jenis;

            if($model->jenis == 'bumil')
            {
                $param = ParamVariable::where('var3','max_birth')->first();
                $minggu = $param->var1 - $request->usia;
                $hari = Carbon::now()->addDays($minggu*7);
                $model->usia_kehamilan =  $request->usia;
                $model->usia_kehamilan_calc =  $request->usia;
                $model->hpl = $hari;
            }else{
                $model->usia_bayi =  $request->usia;
                $model->usia_bayi_calc =  $request->usia;
            }

            $model->tgl_daftar = Carbon::now();
            $model->created_by = Auth::user()->id;
            $model->updated_by = Auth::user()->id;
            $model->save();

            return response()->json(['message' => 'Created Success','data' =>  $model ], 201);
            
        } catch (Exception $e) {
            return response()->json(['message' => 'error. '.$e->getMassage() ,'data' => $e ], 500);
        }
        return response()->json(['message' => 'Something went wrong. '], 500);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request)
    {
        //
         try {
            $model = ProsesPasien::findOrFail($request->id);
            $model->nama = $request->name;
            $model->no_wa = $request->wa;
            $model->jenis = $request->jenis;
            if($model->jenis == 'bumil')
            {
                $param = ParamVariable::where('var3','max_birth')->first();
                $minggu = $param->var1 - $request->usia;
                $hari = Carbon::now()->addDays($minggu*7);
                $model->usia_kehamilan =  $request->usia;
                $model->usia_kehamilan_calc =  $request->usia;
                $model->hpl = $hari;
            }else{
                $model->usia_bayi =  $request->usia;
                $model->usia_bayi_calc =  $request->usia;
            }
            $model->updated_by = Auth::user()->id;
            $model->save();
            return response()->json(['message' => 'Updated Success','data' =>  $model ], 200);
            
        } catch (Exception $e) {
            return response()->json(['message' => 'error. '.$e->getMassage() ,'data' => $e ], 500);
        }
        return response()->json(['message' => 'Something went wrong. '], 500);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Request $request)
    {
        //

        $model = ProsesPasien::findOrFail($request->id);
        $model->is_deleted = 1;
        $model->save();

    }
}
