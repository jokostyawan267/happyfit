<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use ImageOptimizer;
use App\NotifPasien;
use App\ProsesPasien;
use App\KategoryPasien;
use App\JobPromo;
use Auth;
use App\LogPromo;
use Illuminate\Support\Str;
use App\Http\Resources\LogCollection;

class PromoController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        //
        $promo = NotifPasien::where('is_deleted',0);

        if ($request->has('jenis') && $request->jenis != "all") {
            # code...
            $promo = $promo->where('jenis', $request->jenis);
        }
        if($request->_q != '')
        {
            $q = $request->_q;
            # code...
            $promo =$promo->whereRaw("upper(nama) LIKE '%".strtoupper($q)."%'")
            ->orWhereRaw("upper(no_wa) LIKE '%".strtoupper($q)."%'")
            ->orWhereRaw("upper(usia_kehamilan) LIKE '%".strtoupper($q)."%'");
        }
        $promo = $promo->orderBy("created_at","desc")->paginate(7);
        return response()->json(compact('promo'), 200);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
        $model = new NotifPasien;
        $model->judul = $request->name;
        $model->keterangan = $request->deskription;
        $model->created_by = Auth::user()->id;
        $model->updated_by = Auth::user()->id;
        if($request->has('gambar')){
         
            $path = public_path();
            $host = $_SERVER['SERVER_NAME'];
            $destination_dir = $path."/uploads/promo/";
            $base_filename = basename($_FILES["gambar"]["name"]);
            $temp = explode(".", $_FILES["gambar"]["name"]);
            $newfilename = round(microtime(true)) . '.' . end($temp);
            $target_file = $destination_dir . $newfilename;
            if(move_uploaded_file($_FILES["gambar"]["tmp_name"], $target_file)){
                $model->gambar = "uploads/promo/".$newfilename;
            }
         
        }
        if($model->save()){
             return response()->json(compact('model'), 200);
        }

    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request)
    {

        //
        try {
            $model = NotifPasien::findOrFail((int)$request->id);
            $model->judul = $request->judul;
            $model->keterangan = $request->keterangan;
            $model->start = $request->start;
            $model->end = $request->end;
            $model->updated_by = Auth::user()->id;
            if($request->has('gambar')){
                 $model->gambar = $request->gambar;
            }
            $model->save();
            return response()->json(['message' => 'Updated Success','data' =>  $model ], 200);
            
        } catch (Exception $e) {
            return response()->json(['message' => 'error. '.$e->getMassage() ,'data' => $e ], 500);
        }
        return response()->json(['message' => 'Something went wrong. '], 500);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Request $request)
    {

        $model = NotifPasien::findOrFail($request->id);
        $model->is_deleted = 1;
        $model->save();
        //
    }

    public function sendpromo(Request $request){
        // $model = NotifPasien::findOrFail((int)$request->id);
        // $response;

        // if($model->jenis == 'bumil'){
        //     $pasiens = ProsesPasien::where(['is_deleted' => 0,'jenis' => $model->jenis])
        //     ->Where('usia_kehamilan_calc',">=",$model->start)
        //     ->Where('usia_kehamilan_calc',"<=",$model->end);
        //     if($request->has('is_pasien')){
        //         $pasiens = $pasiens->where('is_pasien', $request->is_pasien);
        //     }


        //     foreach ($pasiens->get() as $key => $value) {
        //         # code...
        //         $log = new LogPromo;
        //         $log->to = json_encode($value);
        //         $log->log = json_encode($model);
        //         $log->created_by = Auth::user()->name;
        //         if($log->save()){
        //             $judul_konten = '*'.$model->judul.'*'.' - '.$model->keterangan;
        //             $gambar = isset($model->gambar) ? $model->gambar : null;
        //             $response = self::sendWa($value->no_wa,$judul_konten,$gambar);
        //         }
        //     }
            
        // }else{
        //     $pasiens = ProsesPasien::where(['is_deleted' => 0,'jenis' => $model->jenis]);
        //      if($request->has('is_pasien')){
        //         $pasiens = $pasiens->where('is_pasien', $request->is_pasien);
        //     }
        //     foreach ($pasiens->get() as $key => $value) {
        //         # code...
        //         $log = new LogPromo;
        //         $log->to = json_encode($value);
        //         $log->log = json_encode($model);
        //         $log->created_by = Auth::user()->name;
        //         if($log->save()){
        //             $judul_konten = '*'.$model->judul.'*'.' - '.$model->keterangan;
        //             $gambar = isset($model->gambar) ? $model->gambar : null;
        //             $response = self::sendWa($value->no_wa,$judul_konten,$gambar);
        //         }
        //     }

        // }

          $model = new JobPromo;
          $model->kategori_id = $request->kategori_id;
          $model->promo_id =  $request->id;
          if($model->save())
          return response()->json($model,200);

    }

    public function kategoriPasien(){
       $kategory = KategoryPasien::where('is_deleted','0')->get();
       return response()->json($kategory, 200);
    }

    public function sendWa($to,$pesan,$gambar=null){

        $curl = curl_init();
        $url = 'http://116.203.191.58/api/async_send_message';
        $param["phone_no"] = Str::startsWith($to, '0') ?  Str::replaceFirst("0","+62",$to) : $to;

        $param["key"] = env("KEY_WOO_API");
        $param["message"] = $pesan;
        if(isset($gambar) && $gambar != ""){
            $url = "http://116.203.191.58/api/async_send_image_url";
            $param["url"]  = url('')."/../".$gambar;    
        } 
        curl_setopt_array($curl, array(
          CURLOPT_URL => $url,
          CURLOPT_RETURNTRANSFER => true,
          CURLOPT_ENCODING => '',
          CURLOPT_MAXREDIRS => 10,
          CURLOPT_TIMEOUT => 0,
          CURLOPT_FOLLOWLOCATION => true,
          CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
          CURLOPT_CUSTOMREQUEST => 'POST',
          CURLOPT_POSTFIELDS => json_encode($param),
          CURLOPT_HTTPHEADER => array(
            'Content-Type: application/json'
          ),
        ));
        $response = curl_exec($curl);
        $err = curl_error($curl);
        if ($err) {
          Log::info("cURL Error #:" . $err);
          return null;
        }
        curl_close($curl);
        return $response;
    }

    public function logpromo(Request $request)
    {

        $promo = LogPromo::make();
        if($request->_q != '')
        {
            $q = $request->_q;
            # code...
            $promo =$promo->whereRaw("upper(to) LIKE '%".strtoupper($q)."%'")
            ->orWhereRaw("upper(log) LIKE '%".strtoupper($q)."%'");
        }
        $promo = new LogCollection($promo->orderBy("created_at","desc")->paginate(7));
        return response()->json(compact('promo'), 200);
    }

}
