<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Auth;
use App\ParamVariable\PromoController;
use App\VwDashboardReminToday;
use App\VwDashboardDatangToday;
use App\VwDashboardPromoBulan;
use App\VwDashboardDatangBulan;
use App\VwDashboardPasien;

class UtilController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    public function allParam(Request $request)
    {
        $data = ParamVariable::All();
        return response()->json($data, 200);
    }

    public function storeHpl(Request $request){
        $data = ParamVariable::where('var3','max_birth')->first();
        if(isset($data)){
            $data->var1 = $request->value;
            $data->save();
            $data->updated_by = Auth::user()->name;
            return response()->json($data, 200);
        }

        return response()->json("error not found data", 500);
    }

    public function storeBatasNotifBumil(Request $request){
        $data = ParamVariable::where('var3','max_notif_bumil')->first();
        if(isset($data)){
            $data->var1 = $request->value;
            $data->save();
            $data->updated_by = Auth::user()->name;
            return response()->json($data, 200);
        }
        return response()->json("error not found data", 500);
    }

    public function storeReminderBaby(Request $request){
        $data = ParamVariable::where('var3',$request->kode)->first();
        if(isset($data)){
            $data->var1 = $request->value;
            $data->var2 = $request->pesan;
            $data->var4 = $request->reminder_ulang;
            $data->updated_by = Auth::user()->name;
            $data->save();
            return response()->json($data, 200);
        }
        return response()->json("error not found data", 500);
    }


    public function mobileDashboard(Request $request)
    {
        $data['pasien'] = VwDashboardPasien::first();
        $data['remin_today'] = VwDashboardReminToday::first();
        $data['datang_today'] = VwDashboardDatangToday::first();
        return \Response::json($data);
    }



}
