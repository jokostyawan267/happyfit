<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use Illuminate\Support\Str;
use Illuminate\Support\Facades\Log;
use App\JobPromo;
use Carbon\Carbon;
use App\LogPromo;
use App\ProsesPasien;

class promoCommand extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'command:promo';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Command description';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        //
        $data= JobPromo::where('is_execute','0')->with('promo','kategory')->get();
        foreach ($data as $key => $value) {
          # code...

          $pasiens = ProsesPasien::where(['is_deleted' => 0,'jenis' => $value->kategory->jenis]);
          if($value->kategory->jenis == 'bumil'){
            $pasiens = $pasiens->where('is_pasien',$value->kategory->type == 'Pasien' ? 1: 0)
            ->Where('usia_kehamilan_calc',">=",$value->kategory->start)
            ->Where('usia_kehamilan_calc',"<=",$value->kategory->end)->get();
          }else{
            $pasiens = $pasiens->where('is_pasien',$value->kategory->type == 'Pasien' ? 1: 0)
            ->Where('usia_bayi_calc',">=",$value->kategory->start)
            ->Where('usia_bayi_calc',"<=",$value->kategory->end)->get();
          }
            
          // dd($pasiens);

          foreach ($pasiens as $keys => $value2) {
                # code...
                $log = new LogPromo;
                $log->to = json_encode($value2);
                $log->log = json_encode($value->promo);
                $log->pasien_id = $value2->id;
                $log->promo_id = $value->promo->id;
                if($log->save()){
                    $judul_konten = '*'.$value->promo->judul.'*'.' - '.$value->promo->keterangan;
                    $gambar = isset($value->promo->gambar) ? url($value->promo->gambar) : null;
                    $response = self::sendWa($value2->no_wa,$judul_konten,$gambar);                }
          }
            $value->execute_date = Carbon::now();
            $value->is_execute = 1;
            $value->save();
        }
        // self::sendWa('085649071348','so muach',"https://miro.medium.com/max/9792/1*FoIugGB7Hb0ENjnf_dsaaQ.jpeg");
    }


    public function sendWa($to,$pesan,$gambar=null){

        $curl = curl_init();
        $url = 'http://116.203.191.58/api/async_send_message';
        $param["phone_no"] = Str::startsWith($to, '0') ?  Str::replaceFirst("0","+62",$to) : $to;
        $param["key"] = env("KEY_WOO_API");
        $param["message"] = $pesan;
        if(isset($gambar)){
            $url = "http://116.203.191.58/api/async_send_image_url";
            $param["url"]  = $gambar;
        } 
        Log::info($param);
        curl_setopt_array($curl, array(
          CURLOPT_URL => $url,
          CURLOPT_RETURNTRANSFER => true,
          CURLOPT_ENCODING => '',
          CURLOPT_MAXREDIRS => 10,
          CURLOPT_TIMEOUT => 0,
          CURLOPT_FOLLOWLOCATION => true,
          CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
          CURLOPT_CUSTOMREQUEST => 'POST',
          CURLOPT_POSTFIELDS => json_encode($param),
          CURLOPT_HTTPHEADER => array(
            'Content-Type: application/json'
          ),
        ));
        $response = curl_exec($curl);
        $err = curl_error($curl);
        if ($err) {
          Log::info("cURL Error #:" . $err);
          return null;
        }
        curl_close($curl);
        return $response;
    }
}
