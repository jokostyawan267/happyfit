<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use Illuminate\Support\Str;
use App\ParamVariable;
use App\ProsesPasien;
use Carbon\Carbon;
use App\Reminder;
use App\LogRemin;

class reminderCommand extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'command:reminder';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Command description';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
    
        $pijat  = ParamVariable::where('var3','pijat_bumil')->first();
        $spa = ParamVariable::where('var3','spa_baby')->first();
        $data = Reminder::where('is_deleted',0)->with('kategory')->get();
        foreach ($data as $key => $value) {
          # code...
          
          $remin = ProsesPasien::where('is_pasien',$value->kategory->type)->where("jenis",$value->kategory->jenis);
          if($value->status_kirim == 'before'){

            $waktu_tunggu = $value->kategory->jenis == 'bumil' ? $pijat->var1 : $spa->var1;
            $reminder_ulang = $value->kategory->jenis == 'bumil' ? $pijat->var4 : $spa->var4; 

            $remin = $remin->whereRaw("DATE_FORMAT(last_visit, '%Y-%m-%d') = '".Carbon::now()->subDays($waktu_tunggu - $value->jumlah_hari)->toDateString()."'")->orWhereRaw("DATE_FORMAT(last_visit, '%Y-%m-%d') = '".Carbon::now()->subDays($waktu_tunggu + $reminder_ulang)->toDateString()."'");
          }else{
           
            $remin = $remin->whereRaw("DATE_FORMAT(last_visit, '%Y-%m-%d') = '".Carbon::now()->subDays($value->jumlah_hari)->toDateString()."'");
             // dd( $remin->get());
          }
          // dd(Carbon::now()->subDays($value->jumlah_hari)->toDateString());

          foreach ($remin->get() as $key2 => $value2) {
            # code...
            // dd($value2);
            $value2->reminder_terakhir = Carbon::now();
            $value2->save();

            $log = new LogRemin;
            $log->pasien_id = $value2->id;
            $log->reminder_id = $value->id;
            $log->save();

            self::sendWa($value2->no_wa,$value->keterangan,null);
          }

        }

    }


    public function sendWa($to,$pesan,$gambar=null){

        $curl = curl_init();
        $url = 'http://116.203.191.58/api/async_send_message';
        $param["phone_no"] = Str::startsWith($to, '0') ?  Str::replaceFirst("0","+62",$to) : $to;

        $param["key"] = env("KEY_WOO_API");
        $param["message"] = $pesan;
        if(isset($gambar) && $gambar != ""){
            $url = "http://116.203.191.58/api/async_send_image_url";
            $param["url"]  = url('')."/../".$gambar;    
        } 
        curl_setopt_array($curl, array(
          CURLOPT_URL => $url,
          CURLOPT_RETURNTRANSFER => true,
          CURLOPT_ENCODING => '',
          CURLOPT_MAXREDIRS => 10,
          CURLOPT_TIMEOUT => 0,
          CURLOPT_FOLLOWLOCATION => true,
          CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
          CURLOPT_CUSTOMREQUEST => 'POST',
          CURLOPT_POSTFIELDS => json_encode($param),
          CURLOPT_HTTPHEADER => array(
            'Content-Type: application/json'
          ),
        ));
        $response = curl_exec($curl);
        $err = curl_error($curl);
        if ($err) {
          Log::info("cURL Error #:" . $err);
          return null;
        }
        curl_close($curl);
        return $response;
    }
}
