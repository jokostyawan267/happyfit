<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use App\ParamVariable;
use App\ProsesPasien;
use Carbon\Carbon;

class caculationCommand extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'command:calculation';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Command for calculation add age';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        //
        $Param = ParamVariable::where('var3', 'add_age')->first();
        $max_birth = ParamVariable::where('var3', 'max_birth')->first();
        $pasiens = ProsesPasien::where(['is_deleted' => 0,'jenis' => 'bumil','is_persalinan' => 0])->get();
        foreach ($pasiens as $key => $value) {
            # code...
            $start = Carbon::parse($value->tgl_daftar);
            $left = $start->diffInDays(Carbon::parse(Carbon::now()));
            $value->usia_kehamilan_calc = $value->usia_kehamilan +(int)($left/$Param->var1);
            if($value->usia_kehamilan_calc >= $max_birth->var1){
                $value->is_persalinan = 1;
            }
            $value->save();
        }

    }
}
