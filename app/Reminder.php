<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Reminder extends Model
{
    //
    protected $table = 'reminder';
    protected $primaryKey = 'id';

    public function kategory()
    {
        return $this->belongsTo(KategoryPasien::class,'kategori_id');
    }


}
