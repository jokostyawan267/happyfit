<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class JobPromo extends Model
{
    //
    protected $table = 'job_promo';
    protected $primaryKey = 'id';

    public function kategory()
    {
        return $this->belongsTo(KategoryPasien::class,'kategori_id');
    }

    public function promo()
    {
        return $this->belongsTo(NotifPasien::class,'promo_id');
    }
}
