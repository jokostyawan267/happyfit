<?php

namespace App;

use Illuminate\Database\Eloquent\Model;


class ParamVariable extends Model
{
    //
    protected $table = 'app_parameter';
    protected $primaryKey = 'id';
}
